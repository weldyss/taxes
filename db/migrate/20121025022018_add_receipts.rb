class AddReceipts < ActiveRecord::Migration
  def up
    create_table :receipts do |t|
      t.references :purchase
      t.references :product
      t.integer :quantity
      t.float :total
      t.float :total_with_tax
    end
  end

  def down
    drop_table :receipts
  end
end
