class AddPurchases < ActiveRecord::Migration
  def up
    create_table :purchases do |t|
      t.references :receipt
      t.references :product
      t.float :total
      t.integer :quantity
    end
  end

  def down
    drop_table :purchases
  end
end
