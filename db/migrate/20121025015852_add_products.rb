class AddProducts < ActiveRecord::Migration
  def up
    create_table :products do |t|
      t.string :name
      t.float :price
      t.string :category
      t.boolean :imported

      t.timestamps
    end 
  end

  def down
    drop_table :products
  end
end
