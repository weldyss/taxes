class ChangingReceipts < ActiveRecord::Migration
  def up
    remove_column :receipts, :purchase_id
    remove_column :receipts, :product_id
    remove_column :receipts, :quantity
  end

  def down
    add_column :receipts, :purchase_id, :integer
    add_column :receipts, :product_id, :integer
    add_column :receipts, :quantity, :integer
  end
end
