class Product < ActiveRecord::Base
  attr_accessible :name, :category, :price, :imported

  def imported?
    imported
  end
end
