class Purchase < ActiveRecord::Base
  attr_accessible :quantity, :total, :receipt, :product

  belongs_to :product
  belongs_to :receipt

  def total
    product.price * quantity
  end
 
end
