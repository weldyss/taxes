class Receipt < ActiveRecord::Base

  attr_accessible :purchases

  has_many :purchases

  def total
    value = 0
    purchases.each {|p| value += p.total}
    value
  end

  def total_with_tax
    value = 0.0
    purchases.each do |p|
      value += p.total
      value += p.total * 0.10 if p.product.category == 'goodies'
      value += p.total * 0.05 if p.product.imported?
    end
    value
  end

end
