require 'spec_helper'

describe Receipt do
  
  let(:product) {FactoryGirl.create(:product)}
  let(:product_1) {FactoryGirl.create(:product)}
  
  let(:purchase) {FactoryGirl.create(:purchase, :product => product)}
  

  context "calculating total" do
    it "with one item" do
      subject.purchases << purchase
      expect(subject.total).to eq (product.price * purchase.quantity)
    end

    it "with some items" do
      purchase_1 = FactoryGirl.create(:purchase, 
                                      :product => product,
                                      :quantity => 2)
      subject.purchases << purchase
      subject.purchases << purchase_1
      expect(subject.total).to eq ((product.price * purchase.quantity) + (product_1.price * 2))
    end

    it "with taxes" do
      product_to_tax = FactoryGirl.create(:product, 
                                          :imported => true, 
                                          :category => 'goodies')
      purchase_with_tax = FactoryGirl.create(:purchase, 
                                             :product => product_to_tax)
      subject.purchases << purchase
      subject.purchases << purchase_with_tax
      value1 = product.price * purchase.quantity
      value2 = product_to_tax.price * purchase_with_tax.quantity
      result_no_taxes = (value1).round(2)
      result_with_taxes = (value2 + (value2 * 0.10) + (value2 * 0.05)).round(2)
      expect(subject.total_with_tax).to eq result_no_taxes + result_with_taxes 
    end


  end
end
