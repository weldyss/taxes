require 'spec_helper'

describe Product do

  context "assigning" do
    subject {Product.new(:name      => "My Product",
                         :category  => "whatever",
                         :price     => 12.5,
                         :imported  => false)}

    its(:name) {should == "My Product"}
    its(:category) {should == "whatever"}
    its(:price) {should == 12.5}
    its(:imported?) {should == false}
  end

end
