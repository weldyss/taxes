require 'spec_helper'

describe Purchase do

  it "should belongs to a product" do
    subject.product = Product.new
    expect(subject.product).to be_valid
  end

  it "should belongs to a receipt" do
    subject.receipt = Receipt.new
    expect(subject.receipt).to be_valid
  end

  context "calculating" do
    let(:product1) {Product.create(:name     => "Product 1",
                                   :category => "goodies",
                                   :imported => "false",
                                   :price    => 15.5)}
    
    it "total price to products" do
      subject.product = product1
      subject.quantity = 3
      expect(subject.total).to eq 46.5
    end
  end
end
