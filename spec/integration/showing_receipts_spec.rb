require 'spec_helper'

describe 'Receipts' do
  
  
  context 'showing receipts' do  
    let(:product) {FactoryGirl.create(:product, :imported => true)}
    let(:purchase) {FactoryGirl.create(:purchase, :product => product)}

    before do
      @receipt = Receipt.create(:purchases => [purchase])
      visit root_path
    end

    it 'should show outputs' do
      page.should have_table('receipts', :rows => [["#{purchase.quantity}", "imported #{product.name}", "#{product.price}"]])
    end

  end
end
