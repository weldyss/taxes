require 'factory_girl'

FactoryGirl.define do
  
  factory :product do
    name "My product"
    category "medical"
    imported false
    price 15.0
  end

  factory :purchase do
    product {FactoryGirl.build(:product)}
    quantity [1,2,3,4,5].sample
  end
end
